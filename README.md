# Index

[Deutsch](#datenschutzerklärung)
[English](#privacy-policy)

---

# Datenschutzerklärung

## Dienstanbieter

André Schwarzer
datenschutz@schwarzerit

### Erstreckungsbereich

*Es gilt immer die aktuellste Datenschutzerklärung.*

Da durch neue Technologien und die ständige Weiterentwicklung Änderungen 
an dieser Datenschutzerklärung vorgenommen werden können, empfehlen wir Ihnen 
sich die Datenschutzerklärung in regelmäßigen Abständen wieder durchzulesen.

#### Apps

##### Android

- TG Sticker Für WA (ab Version 0.0.1)

### personenbezogene Daten

Es werden keine personenbezogenen Daten erhoben.

---

# Privacy Policy

## Service provider

André Schwarzer
privacy-policy@schwarzerit

### Impact

*Only the most recent privacy policy is valid.*

As changes to this Privacy Policy may be made through new technology and 
ongoing development, we encourage you to review the Privacy Policy periodically.

#### Apps

##### Android

- TG Stickers For WA (from version 0.0.1)

### personal Data

No personal data will be collected.